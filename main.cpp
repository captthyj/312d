#include<iostream>
#include<fstream>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>

using namespace std;

#define For(i,a,b) for(i=a;i<b;i++)
#define For2(i,a,b) for(i=a;i>b;i--)
int beginTime;
int startTime=0;
int serverTime=0;
int nowTime=0;
int change[4][2]={0};
class Service;
void cout(Service service);

class Bill
{
public:
    int room_id;
    float fee;
    int serviceTime;
    void upFee(float fee);
    Bill();
};
Bill::Bill()
{
    room_id=0;
    fee=0;
    serviceTime=0;
}
void Bill::upFee(float fee){}

class Details
{
public:
    int  room_id;
    int changeTemTimes ; //调温次数
    int changeWindTimes;
    void createDetails(int room_id);//创建详单
    void upDetails(int room_id);//更新详单
    Details();
};
void Details::createDetails(int room_id){}
void Details::upDetails(int room_id){}
Details::Details()
{
    room_id=0;
    changeTemTimes=0;
    changeWindTimes=0;
}

class WeekReport
{
public:
    int room_id;
    int airConTime;
    int totalFee;
    int scheduleTimes;
    int detailsCount;
    int changeTemTimes;
    int changeWindTimes;
    void putDetails();//保存周报到文件
    WeekReport();

};
WeekReport::WeekReport()
{
    room_id=0;
    airConTime=0;
    totalFee=0;
    scheduleTimes=0;
    detailsCount=0;
    changeWindTimes=0;
    changeWindTimes=0;
}
void WeekReport::putDetails()
{

}

class List
{
public:
    int room_id;
    int startTime;
    int targetTem;
    int windSpeed;
    int waitTime;
    int serverTime;
    List();
};
List::List()
{
    room_id=0;
    serverTime=0;
    targetTem=0;
    windSpeed=0;
    waitTime=0;
    serverTime=0;
}

class Service
{
public:
    int room_id;
    int state;//（0:关机/1:开机工作/2:待机/3:等待）
    int serverStartTime;// （服务最开始时间）
    int nowStartTime;//     （当前服务开始时间）
    int standbyStartTime ;//     (待机开始时间 )
    int nowStandyTime;//    （此次待机总时间）
    int mode;//     （空调工作模式）
    int targetTem;// （目标温度）
    int nowTem;// （当前温服）
    int windSpeed;// (风速)
    int serverTime;// （服务总时长）
    int nowServerTime;// （当前调度时长）
    int highTem;// （最高温度）
    int lowTem;// （最低温度）
    float hFee;// （高速费率）
    float mFee;// （中费率）
    float lFee;// （低费率）
    int defaultTem;// （初始默认目标温度）
    Bill bill;
    Details details;
    Service();
    Service(int highTem,int lowTem,float hFee,float mFee,float lFee);
    void upFeeDate();//更新账单、详单、报表
    void  upState();//更新各状态（包括开机和待机状态的所有涉及到的属性修改）
    void  getBill();//获得账单
};
Service::Service()
{
    room_id=0;
    state=0;
    serverStartTime=0;
    nowStartTime=0;
    standbyStartTime=0;
    nowStandyTime=0;
    serverTime=0;
    nowServerTime=0;
    windSpeed=0;
    fstream fa;
    fa.open("../service.txt");
    int tem;
    fa>>tem;
    mode=tem;
    fa>>tem;
    targetTem=tem;
    fa>>tem;
    nowTem=tem;
    fa>>tem;
    highTem=tem;
    fa>>tem;
    lowTem=tem;
    float t;
    fa>>t;
    hFee=t;
    fa>>t;
    mFee=t;
    fa>>t;
    lFee=t;






    defaultTem=25;
}
Service::Service(int ht, int lt, float hf, float mf, float lf)
{
    highTem=ht;
    lowTem=lt;
    hFee=hf;
    mFee=mf;
    lFee=lf;
}
void Service::upFeeDate(){}
void Service::upState()
{
    int i;
    For(i,0,4)
    {
        if(change[i][0]==room_id)
        {
            if(change[i][1]==1)
            {

            }
            break;
        }
    }
}
void Service::getBill() {}

class Scheduler
{
public:
    int roomNum;
    int startTime;
    int conditionNum=3;
    List waitList[4];
    List serverList[4];
    int waitListLength=0;
    int serverListLength=0;
    Service service[4];
    void createNewList(int room_id);//list创建
    void setAirConState(int highTem,int lowTem,int defaultTem,float hFee,float mFee,float lFee);
    void schedule();//调度
    Scheduler();

};
Scheduler::Scheduler()
{
    roomNum=0;
    serverTime=0;
    startTime=0;
    waitListLength=0;
    serverListLength=0;
}
void Scheduler::createNewList(int room_id)//房间开启之后，打开空调时调用
{
    int i;
    For(i,0,4)
    {
        if(service[i].room_id==room_id)
            break;
    }
    if (serverListLength<conditionNum)
    {
        serverList[serverListLength].room_id=service[i].room_id;
        serverList[serverListLength].startTime=nowTime;
        serverList[serverListLength].targetTem=service[i].targetTem;
        serverList[serverListLength].windSpeed=service[i].windSpeed;
        serverList[serverListLength].waitTime=0;
        serverList[serverListLength].serverTime=0;
        serverListLength+=1;
    }
    else
    {
        waitList[serverListLength].room_id=service[i].room_id;
        waitList[serverListLength].startTime=nowTime;
        waitList[serverListLength].targetTem=service[i].targetTem;
        waitList[serverListLength].windSpeed=service[i].windSpeed;
        waitList[serverListLength].waitTime=0;
        waitList[serverListLength].serverTime=0;
        waitListLength+=1;
    }

}
void Scheduler::setAirConState(int highTem,int lowTem,int defaultTem,float hFee,float mFee,float lFee)
{

}
void Scheduler::schedule()
{
    if (waitListLength==0)
        return;
    int i;
    int sort[4][2];
    int tem,num;
    int room_id=waitList
    For(i,0,serverListLength)
    {

    }

}







//经理调用该类 ok
class CheckController
{
public:
    void putWeekReport(int  room_id);
    void putAllReport();
};
void CheckController::putWeekReport(int room_id) {}
void CheckController::putAllReport() {}

//前台调用该类 ok
class FeeController
{
public:
    void getBill(int room_id);
    void getDetails(int room_id);
};
void FeeController::getBill(int room_id) {}
void FeeController::getDetails(int room_id) {}

//管理员 调用 *
class AirController
{
public:

    int state;//(空调状态)
    int highTem;//（最高温度）
    int lowTem;//（最低温度）
    int defaultTem;//（初始默认目标温度）
    float hFee;//（高速费率）
    float mFee;//（中费率）
    float lFee;//（低费率）
    Scheduler scheduler;
    AirController();
    void powerOn();
    void powerOff();
    void checkController(int room_id);//监视房间状态
    void setAirConState();
    void reauestOn(int room_id);
    void changeTem(int room_id,int targetTem);
    void changeWindSpeed(int room_id,int winSpeed);
    void requstFee(int room_id);
};
AirController::AirController()
{
    state=0;
    highTem=0;
    lowTem=0;
    defaultTem=0;
    hFee=0;
    mFee=0;
    lFee=0;

}
void AirController::powerOn()//开中央空调
{
    state=1;
    startTime=nowTime;
}
void AirController::powerOff()//关中央空调
{
    state=0;
}
void AirController::checkController(int room_id)
{
    int  i;
    int  j=0;
    For(i,0,4)
    {
        if (scheduler.service[i].room_id == room_id)
        {
            j=1;
            break;
        }
    }
    if(j==1)
        std::cout<<"房间"<<room_id<<"状态信息："<<endl;
    else
        std::cout<<"该房间不存在";
}
void AirController::setAirConState()
{
    std::cout<<"请您依次输入最高温度、最低温度、高费率、中费率 、低费率"<<endl;
    int ht,lt,hf,mf,lf;
    cin>>ht>>lt>>hf>>mf>>lf;
    int i;
    For(i,0,scheduler.roomNum)
    {
        scheduler.service[i].highTem=ht;
        scheduler.service[i].lowTem=lt;
        scheduler.service[i].hFee=hf;
        scheduler.service[i].lFee=lf;
        scheduler.service[i].mFee=mf;
    }
}
void AirController::reauestOn(int room_id)
{
    scheduler.createNewList(room_id);
}
void AirController::requstFee(int room_id)
{
    int i;
    int j=0;
    For(i,0,scheduler.roomNum)
    {
        if (room_id==scheduler.service[i].room_id)
        {
            j=1;
            break;
        }
    }
    if(j==1)
    {
        std::cout<<scheduler.service[i].bill.fee;


    }
    else
        std::cout<<"该房间不存在";
}
void AirController::changeTem(int room_id, int targetTem)
{
    int i,j=0;
    For(i,0,scheduler.roomNum)
    {
        if(room_id==scheduler.service[i].room_id)
        {
            j=1;
            break;
        }
    }
    if(j==1)
    {
        scheduler.service[i].targetTem=targetTem;
    }
    else
        std::cout<<"该房间不存在";
}
void AirController::changeWindSpeed(int room_id, int winSpeed)
{
    int i,j=0;
    For(i,0,scheduler.roomNum)
    {
        if(room_id==scheduler.service[i].room_id)
        {
            j=1;
            break;
        }
    }
    if(j==1)
    {
        scheduler.service[i].windSpeed=winSpeed;
    }
    else
        std::cout<<"该房间不存在";

}

int main()
{
    AirController ac;




}




void cou(Service service)
{
    std::cout<<"房间号："<<service.room_id<<endl;
    std::cout<<"当前温度："<<service.nowTem<<endl;
    std::cout<<"目标温度："<<service.targetTem<<endl;
    std::cout<<"风速："<<service.windSpeed<<endl;
    std::cout<<"空调状态："<<service.state<<endl;
    std::cout<<"空调模式："<<service.mode<<endl;
    std::cout<<"服务开始时间："<<service.serverStartTime<<endl;
    std::cout<<"房间费用："<<service.bill.fee<<endl;
    std::cout<<"高费率："<<service.hFee<<endl;
    std::cout<<"中费率："<<service.mFee<<endl;
    std::cout<<"低费率："<<service.lFee<<endl;

}
void putmenu()
{
    std::cout<<"请输入选择";
    std::cout<<""<<endl;

}







