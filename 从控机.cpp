#include<iostream>
#include<fstream>
#include <stdlib.h>
#include <algorithm>
#include <string.h>
#include <math.h>
using namespace std;
#define For(i,a,b) for(i=a;i<b;i++)
#define For2(i,a,b) for(i=a;i>b;i--)

int startTime=0;
int nowTime=0;
int lastChangeTime=0;
int ce=1;
class Room
{
public:
    Room(/* args */);

    int room_id ;
    int state;//（0:关机/1:开机工作/2:待机/3:等待）
    int serverStartTime ;//（服务最开始时间）
    int mode ;   //（空调工作模式0:制冷/1:制热）
    int targetTem;//（;目标温度）
    int nowTem;//（当前温服）
    int windSpeed;
    int fee;
    void newTem(int nowTem,int targetTem,int windSpeed);
    ~Room();
};

Room::Room(/* args */)
{
}
void Room::newTem(int nowTem1,int targetTem1,int windSpeed1)
{
    switch (windSpeed1)
    {
        case 1:
        {
            while((nowTime-lastChangeTime)>=6)
            {

                if(mode==1)
                {
                    nowTem+=1;
                }
                else
                {
                    nowTem-=1;
                }
                lastChangeTime+=6;
            }
            lastChangeTime=nowTime;
            break;
        }
        case 2:
        {
            while((nowTime-lastChangeTime)>=4)
            {
                if(mode==1)
                {
                    nowTem+=1;
                }
                else
                {
                    nowTem-=1;
                }
                lastChangeTime+=4;
            }
            lastChangeTime=nowTime;
            break;
        }
        case 3:
        {
            while((nowTime-lastChangeTime)>=2)
            {
                if(mode==1)
                {
                    nowTem+=1;
                }
                else
                {
                    nowTem-=1;
                }
                lastChangeTime+=2;
            }
            lastChangeTime=nowTime;
            break;
        }
        default:
        {
            cout<<"风速出错";
            windSpeed=windSpeed1%3+1;
            break;
        }
    }

}
Room::~Room()
{
}
void cou(Room  room);
void putmenu()
{
    cout<<"调整空调按1"<<endl<<"关闭按2"<<"不更改按3"<<endl<<"关机"<<endl;
}
int main()
{
    Room room;
    time_t now;
    time(&now);
    startTime=now;
    int j;
    fstream fa;
    fa.open("../room.txt");
    fa>>room.room_id;
    fa>>room.state;
    fa>>j;
    fa>>room.mode;
    fa>>room.targetTem;
    fa>>room.nowTem;
    fa>>room.windSpeed;
    fa>>room.fee;
    while(true)
    {
        putmenu();
        int i;
        cin>>i;
        if(i==1)
        {
            room.state=1;
            cou(room);
            cout<<"请输入目标温度、风速、工作模式"<<endl;
            cin>>room.targetTem>>room.windSpeed>>room.mode;
            time(&now);
            room.serverStartTime=now-startTime;//空调开始工作时间
            lastChangeTime=now;
        }
        else if(i==2)
        {
            room.state=0;
        }
        else if(i==3)
        {
            time(&now);
            nowTime = now;
            room.newTem(room.nowTem, room.targetTem, room.windSpeed);
            cou(room);
        }
        else break;
    }

}
void cou(Room  room)
{
    cout<<endl;
    cout<<"房间号："<<room.room_id;
    cout<<endl;
    cout<<"空调状态："<<room.state;
    cout<<endl;
    cout<<"房间开始时间："<<room.serverStartTime;
    cout<<endl;
    cout<<"空调模式："<<room.mode;
    cout<<endl;
    cout<<"目标温度："<<room.targetTem;
    cout<<endl;
    cout<<"当前温度："<<room.nowTem;
    cout<<endl;
    cout<<"风速："<<room.windSpeed;
    cout<<endl;
    cout<<"费用："<<room.fee;
    cout<<endl;
}